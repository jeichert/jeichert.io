import gulp from 'gulp';

gulp.task('svg', function() {
  return gulp.src('src/svg/*.svg')
    .pipe(gulp.dest('staging/img'))
})
