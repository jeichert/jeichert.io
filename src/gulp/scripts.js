import gulp from 'gulp';
import jshint from 'gulp-jshint';
import uglify from 'gulp-uglify';
import babel from 'gulp-babel';

gulp.task('scripts', function() {
    return gulp.src('src/scripts/*.js')
        .pipe(jshint())
        .pipe(babel())
        .pipe(jshint.reporter("default"))
        .pipe(uglify())
        .pipe(gulp.dest('staging/js'));
});
