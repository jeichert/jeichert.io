import gulp from 'gulp';
import sync from 'browser-sync';

const reload = sync.reload;

gulp.task('build:content', ['reference:content'], reload);
gulp.task('build:all', ['reference:all'], reload);
gulp.task('build:publish', ['reference:publish']);
