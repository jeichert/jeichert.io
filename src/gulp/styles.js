import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import minifyCss from 'gulp-minify-css';

gulp.task('styles', function() {
  return gulp.src('src/styles/*.scss')
    .pipe(sass())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(minifyCss({advanced: false}))
    .pipe(gulp.dest('staging/css'))
})
