import gulp from 'gulp';
import childProcess from 'child_process';
import gutil from 'gulp-util';
import path from 'path';
import del from 'del';

var exec = childProcess.execSync;
process.chdir('../');

function hugo(drafts) {
  var src = path.join(process.cwd());
  var dst = path.join(process.cwd(), 'public');

  gutil.log(`src: ${src} dst: ${dst}`);
  var cmd = `hugo --config=config.yaml -s ${src} -d ${dst}`;

  if (drafts) {
    cmd += ' --buildDrafts=true --verbose=true --baseUrl="http://localhost:3000" ';
  }

  var result = exec(cmd, {encoding: 'utf-8'});
  gutil.log(`hugo: \n ${result}`);
}

gulp.task('hugo:draft', function() {
  hugo(true);
});

gulp.task('hugo:all', ['revision'], function() {
  hugo(true);
});

gulp.task('hugo:delete', ['revision'], function() {
  var dst = path.join(process.cwd(), 'public');
  del.sync(dst);
});

gulp.task('hugo:live', ['hugo:delete'], function() {
  hugo(false);
})
