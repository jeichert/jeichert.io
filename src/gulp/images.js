import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import changed from 'gulp-changed';

gulp.task('images', function() {
  return gulp.src('src/img/*.*')
    .pipe(changed('staging/img'))
    .pipe(imagemin())
    .pipe(gulp.dest('staging/img'))
});
