import requireDir from 'require-dir';

requireDir('./gulp', {recursive: true});

// import gulp from 'gulp';
// import util from 'gulp-util';
// import sass from 'gulp-sass';
// import concat from 'gulp-concat';
// import jshint from 'gulp-jshint';
// import sourcemaps from 'gulp-sourcemaps';
// import autoprefixer from 'gulp-autoprefixer';
//
// const input = {
//   'styles': 'styles/**/*.scss',
//   'scripts': 'scripts/**/*.js'
// };
//
// const output = {
//   'styles': '../static/bundle',
//   'scripts': '../static/bundle'
// };
//
// function handle(error) {
//   console.error(error.toString())
//   this.emit('end')
// }
//
// gulp.task('jshint', () => {
//     return gulp.src(input.scripts)
//       .pipe(jshint())
//       .pipe(jshint.reporter('jshint-stylish'))
// });
//
// gulp.task('js', () => {
//   return gulp.src(input.scripts)
//     .pipe(sourcemaps.init())
//     .pipe(concat('bundle.js'))
//     .on('error', handle)
//     .pipe(sourcemaps.write('.'))
//     .pipe(gulp.dest('bundle'))
//     .pipe(gulp.dest(output.scripts))
// });
//
// gulp.task('css', () => {
//   return gulp.src(input.styles)
//     .pipe(sourcemaps.init())
//     .pipe(concat('bundle.css'))
//     .pipe(sass())
//     .pipe(autoprefixer({
//       browsers: ['last 2 versions']
//     }))
//     .on('error', handle)
//     .pipe(sourcemaps.write('.'))
//     .pipe(gulp.dest('bundle'))
//     .pipe(gulp.dest(output.styles))
// });
//
// gulp.task('watch', () => {
//   gulp.watch(input.scripts, ['js'])
//   gulp.watch(input.styles, ['css'])
// });
//
// gulp.task('default', ['jshint', 'js', 'css', 'watch'])
